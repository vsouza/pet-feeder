typedef struct {
    int type;
    char* nickname;
} MyPet;

typedef struct {
    int cycles;
    int cycleTime;
} FeedAmount;

typedef struct {
    int hour;
    int minute;
    bool feeded;
    FeedAmount amount;
} Schedule;

typedef struct {
    char* ssid;
    char* password;
    MyPet myPet;
} SystemConfig;

