#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Brzo.h" // alias for `#include "SSD1306Wire.h"`

// Include custom images
#include "dog.h"
#include "cat.h"

const int PET_DOG = 1;
const int PET_CAT = 2;

// Initialize the OLED display using brzo_i2c
// D3 -> SDA
// D5 -> SCL
// SSD1306Brzo display(0x3c, D3, D5);
// or
// SH1106Brzo  display(0x3c, D3, D5);

// Initialize the OLED display using Wire library
SSD1306Brzo  display(0x3c, D1, D5);

void setupDisplay(){
    // Initialising the UI will init the display too.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}

void drawText(OLEDDISPLAY_TEXT_ALIGNMENT alignment, char* font, int x, int y, String text){
    display.clear();
    display.setTextAlignment(alignment);
    display.setFont(font);
    display.drawString(x, y, text);
    display.display();
}

void drawText(OLEDDISPLAY_TEXT_ALIGNMENT alignment, int x, int y, String text){
    display.clear();
    display.setTextAlignment(alignment);
    display.setFont(ArialMT_Plain_16);
    display.drawString(x, y, text);
    display.display();
}

void drawText(int x, int y, String text){
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_16);
    display.drawString(x, y, text);
    display.display();
}

void drawText(String text){
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_10);
    display.drawString(64, 5, text);
    display.display();
}

void drawPet(int pet, String petName){
    display.clear();
    display.drawXbm(43, 6, 40, 40, pet == PET_DOG ? dog_bits : cat_bits);
    display.display();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawString(64, 48, petName);
    display.display();
}

void drawProgressBarDemo(int counter) {
    display.clear();
    int progress = (counter / 5) % 100;
    // draw the progress bar
    display.drawProgressBar(0, 32, 120, 10, progress);

    // draw the percentage as String
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 15, String(progress) + "%");
    display.display();
}

