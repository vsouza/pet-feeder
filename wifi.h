#include <ESP8266WiFi.h>          //ESP8266 Core WiFi Library (you most likely already have this in your sketch)
#include "config_server.h"

bool scanNetworks(boolean isConnected){
    if(!isConnected){
        WiFi.mode(WIFI_STA);
        WiFi.disconnect();
        delay(100);
    
        int n = WiFi.scanNetworks();
        if (n > 0){
            int buffer_size = 15; // "{\"networks\":[";
            for (int i = 0; i < n; ++i){
                buffer_size += 2 + WiFi.SSID(i).length(); // double " plus ssdi length
                if(i<n-1){
                    buffer_size += 1; // , char
                }
            }
            char json[buffer_size];
            for (int i = 0; i < n; ++i)
            {
                WiFi.SSID(i).toCharArray(json,  WiFi.SSID(i).length());
            }
            setupConfigServer();
            startConfigClient(json);  
        }     
    }

    return isConnected;
}

bool setupWifi(char* ssid, char* password){
    WiFi.begin(ssid, password);
    int counter = 0;
    while (WiFi.status() != WL_CONNECTED && ++counter < 10) {
        delay(500);
        Serial.print(".");
    }

    return scanNetworks(WiFi.status() == WL_CONNECTED);
}
