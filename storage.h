#include <ArduinoJson.h>
#include <FS.h>

const char *configFileName = "/config.txt";
SystemConfig _systemConfig;

Schedule* CURRENT_TRIGGER;
MyPet MY_PET;

void createConfigFile(){
    File configFile = SPIFFS.open(configFileName,"w+");
    
    //Verifica a criação do arquivo
    if(!configFile){
      Serial.println("Erro ao criar arquivo!");
    }
    
    configFile.close();
}

void writeConfigFile(const char* json){
    File wFile = SPIFFS.open(configFileName,"a+");
    if(!wFile){
      Serial.println("Erro ao abrir arquivo!");
    } else {
      wFile.println(json);
    }
    wFile.close();
}

void loadConfigurations(){
    const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + 160;
    /*File rFile = SPIFFS.open(configFileName,"r");
    if(!SPIFFS.exists(configFileName)){
        createConfigFile();
        writeConfigFile("{\"ssid\":\"\",\"password\":\"\",\"myPet\":{\"type\":1,\"nickname\":\"My Pet\"}}");
    }

    DynamicJsonBuffer jsonBuffer(bufferSize);
    // Parse the root object
    JsonObject &root = jsonBuffer.parseObject(rFile);
    
    _systemConfig.ssid = root["ssid"];
    strlcpy(_systemConfig.ssid,
          root["ssid"] | "",  // <- source
          sizeof(_systemConfig.ssid)
    );
    strlcpy(_systemConfig.password,
          root["password"] | "",  // <- source
          sizeof(_systemConfig.password)
    );
    _systemConfig.myPet.type = root["myPet"]["type"];
    strlcpy(_systemConfig.myPet.nickname,
          root["myPet"]["nickname"] | "",  // <- source
          sizeof(_systemConfig.myPet.nickname)
    );
    
    rFile.close();*/
}

Schedule TRIGGERS[7][4] = {
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
    {
        {0, 1, false, { 5, 1}},
        {6, 0, false, { 5, 1}},
        {12, 0, false, { 2, 1}},
        {18, 0, false, { 2, 1}}
    },
};

MyPet* getMyPet(){
    return &MY_PET;
}

void updateMyPet(int pType, char* pNickname){
    MY_PET.type = pType;
    MY_PET.nickname = pNickname;
}

Schedule* getTriggers(int weekDay){
  return TRIGGERS[weekDay];
}

Schedule* getCurrentTrigger(){
    return CURRENT_TRIGGER;
}

void setCurrentTrigger(Schedule* pTrigger){
    CURRENT_TRIGGER = pTrigger;
}

void updateCurrentTrigger(bool pFeeded){
    CURRENT_TRIGGER->feeded = pFeeded;
}
