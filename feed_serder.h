#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
//#include "relay_control.h"

ESP8266WebServer feedSerder (80);

void returnFail(int httpStatus, String message){
    feedSerder.sendHeader("Connection", "close");
    feedSerder.sendHeader("Access-Control-Allow-Origin", "*");
    feedSerder.send(httpStatus, "text/plain", message + "\r\n");
}

void returnOK()
{
  feedSerder.sendHeader("Connection", "close");
  feedSerder.sendHeader("Access-Control-Allow-Origin", "*");
  feedSerder.send(200, "text/plain", "OK\r\n");
}

void handleRoot(){
    feedSerder.send ( 200, "text/html", String("<!doctype html><html lang=\"en\"><head> <meta charset=\"UTF-8\"> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"> <meta name=\"description\" content=\"Light Switcher v0.1b\"> <title>Light Switcher</title> <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\"> <script defer src=\"https://use.fontawesome.com/releases/v5.0.6/js/all.js\"></script></head><body> <br/><br/> <div class=\"container\"> <div class=\"row text-center\"> <div class=\"col-12 col-md-12\"> <h3> Light Switcher <small class=\"text-muted\">v0.1b</small> </h3> <p class=\"align-center display-5 pb-3\"> This is a beta version of the light switcher. </p></div></div></div><br/> <form id=\"the_form\" action=\"\" method=\"post\"><input type=\"hidden\" id=\"switch\" name=\"switch\" value=\"\"/></form><div class=\"container\"> <div class=\"row text-center\"> <div class=\"card col-6 col-md-6 border-0\"> <div class=\"card-img\"> <div style=\"font-size:3em; color:" + String(getChannelValue(1) ? "Green" : "Tomato") + "\"> <i class=\"far fa-lightbulb\"></i> </div></div><div class=\"card-box\"> <h4 class=\"card-title py-3 display-5\"> Channel 1</h4> <p class=\"display-7\"> Turn on the channel 1.</p><div class=\"align-center\"><button type=\"button\" onclick=\"do_submit('ch1', '" + String(getChannelValue(1) ? "off" : "on") + "')\" class=\"btn btn-md btn-secondary display-4\">Toggle</button></div></div></div><div class=\"card col-6 col-md-6 border-0\"> <div class=\"card-img\"> <div style=\"font-size:3em; color:" + String(getChannelValue(2) ? "Green" : "Tomato") + "\"> <i class=\"far fa-lightbulb\"></i> </div></div><div class=\"card-box\"> <h4 class=\"card-title py-3 display-5\"> Channel 2</h4> <p class=\"display-7\"> Turn on the channel 2.</p><div class=\"align-center\"><button type=\"button\" onclick=\"do_submit('ch2', '" + String(getChannelValue(2) ? "off" : "on") + "')\" class=\"btn btn-md btn-secondary display-4\">Toggle</button></div></div></div></div></div><script type=\"text/javascript\">function do_submit(channel, val){document.getElementById(\"switch\").value=val; document.getElementById(\"the_form\").action=\"/\" + channel; document.getElementById(\"the_form\").submit();}</script><script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script></body></html>") );
}

void handleSwitch(int channel){
    
    if(!feedSerder.hasArg("switch")){
        returnFail(405, "Provide the switch argument option");
        return;
    }
    
    Serial.println ( "Received switch for channel " + String(channel) + " with switch = " + feedSerder.arg("switch"));
    Serial.println ( "Channel " + String(channel) + " old value: " + String(getChannelValue(channel)) );
    
    doTrigger(channel, feedSerder.arg("switch") == "on");

    Serial.println ( "Channel " + String(channel) + " new value: " + String(getChannelValue(channel)) );
    
    handleRoot();
}

void startfeedSerder(){
    // start webserver
    Serial.println ( "Starting Relay Server" );

    if ( MDNS.begin ( "lightswitcher" ) ) {
        Serial.println ( "MDNS responder started" );
    }

    setupRelayControl();

    Serial.println ( "Preparing for http calls..." );
    feedSerder.on( "/", handleRoot);
    feedSerder.on ( "/ch1", []() {
        handleSwitch(1);
    } );
    feedSerder.on ( "/ch2", []() {
        handleSwitch(2);
    } );
    feedSerder.begin();
    
    doTrigger(1,false);
    doTrigger(2,false);
    
    Serial.println ( "Relay HTTP Server Started" );
}

void handleRelayClient(){
    feedSerder.handleClient();
}

