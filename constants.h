const char STR_WIFI_SETUP_FAIL[] PROGMEM = {"wifi setup fail"};
const char STR_CONFIGURING_WIFI[] PROGMEM = { "Configuring WiFI" };
const char STR_WIFI_SETUP_SUCCESS[] PROGMEM = { "WiFI setup success" };
