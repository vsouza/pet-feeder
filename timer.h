// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 rtc;

void setupTimer (boolean shouldAdjustTime) {
  Wire.pins(0, 2);// yes, see text
  Wire.begin(0,2);// 0=sda, 2=scl
  
  if (! rtc.begin()) {
    drawText("Error 01: Couldn't find RTC");
    while (1);
  }

  if (shouldAdjustTime)
  {
      DateTime now = rtc.now();
      DateTime toAdjust = DateTime(F(__DATE__), F(__TIME__));
      
      // following line sets the RTC to the date & time this sketch was compiled
      rtc.adjust(toAdjust);
      delay(3000);
      // This line sets the RTC with an explicit date & time, for example to set
      // January 21, 2014 at 3am you would call:
      // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
}

boolean isSchedulerTime () {
    DateTime now = rtc.now();

    Schedule* dailyTriggers = getTriggers(now.dayOfTheWeek());
    int triggerCount = sizeof(dailyTriggers);
    
    while(--triggerCount >=0){
      
        if(now.hour() == dailyTriggers[triggerCount].hour 
            && now.minute() == dailyTriggers[triggerCount].minute
            && !dailyTriggers[triggerCount].feeded) {
            setCurrentTrigger(&dailyTriggers[triggerCount]);
            return true;
        }
        
    }
    
    return false;
}
