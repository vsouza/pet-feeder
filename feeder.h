#include <Servo.h>

#define servo1Pin D2 // Define the NodeMCU pin to attach the Servo

Servo myservo; 

void setupFeeder(){
    // nothing now
    myservo.attach(servo1Pin);
    myservo.write(180);
    delay(500);  
    myservo.detach();
    delay(50);
}

void feed(bool storage) {
    myservo.attach(servo1Pin);

    int cyclesQtt = 12;
    int cycleTimeQtt = 1;
    
    if(storage){
      cyclesQtt = getCurrentTrigger()->amount.cycles;
      cycleTimeQtt = getCurrentTrigger()->amount.cycleTime;
    }
    
    for(int count=0; count<cyclesQtt; count++){
        myservo.write(30);
        delay(500 * cycleTimeQtt);
        myservo.write(180);
        delay(1500);  
    }
    
    myservo.detach();
    delay(50);
    
    if(storage){
      updateCurrentTrigger(true); 
    }
    
}
