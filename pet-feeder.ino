#include "constants.h"
#include "datastructures.h"
#include "storage.h"
#include "display.h"
#include "wifi.h"
#include "feeder.h"
#include "timer.h"

void startWifi(){
    delay(1000);
    drawText(STR_CONFIGURING_WIFI);
    if(setupWifi("1234", "1234")){
        drawText(STR_WIFI_SETUP_SUCCESS); 
    } else {
        drawText(STR_WIFI_SETUP_FAIL);
    }
}

void setup() {
    Serial.begin(57600);
    //setupDisplay();
    setupFeeder();
    setupTimer(false);
    
    //feed(false);
    
    //startWifi();
    //drawPet(PET_CAT, getMyPet()->nickname);
}

void loop() {
    if(isSchedulerTime()){
        //drawText("Comida!");
        feed(true);
        //drawText("Acabou!");
        delay(1000);
        //drawPet(PET_CAT, getMyPet()->nickname);
    }
}
 
